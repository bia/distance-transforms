@options
@packages
-classpath
'/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/icy-kernel/2.4.2/icy-kernel-2.4.2.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/blocks/5.3.0/blocks-5.3.0.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/protocols/3.2.2/protocols-3.2.2.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/javadocparser/1.0.1/javadocparser-1.0.1.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/javacl/1.0.6/javacl-1.0.6.pom:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/ezplug/3.21.0/ezplug-3.21.0.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/icy-insubstantial/7.3.10/icy-insubstantial-7.3.10.jar:/home/kitwaii/.m2/repository/net/imagej/ij/1.53r/ij-1.53r.jar:/home/kitwaii/.m2/repository/org/swinglabs/swingx/swingx-all/1.6.5-1/swingx-all-1.6.5-1.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/3d-mesh-roi/1.4.7/3d-mesh-roi-1.4.7.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/quickhull/1.0.2/quickhull-1.0.2.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/vecmath/1.6.1/vecmath-1.6.1.jar:/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/icy-bioformats/6.9.1/icy-bioformats-6.9.1.jar'
-encoding
'UTF-8'
-protected
-source
'1.8'
-sourcepath
'/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/src/main/java:/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/generated-sources/annotations'
-author
-bottom
'Copyright &#169; 2020&#x2013;2022 <a href="https://pasteur.fr">Institut Pasteur</a>. All rights reserved.'
-charset
'UTF-8'
-d
'/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target'
-docencoding
'UTF-8'
-doctitle
'ROI to Distance Map 3.0.0 API'
-linkoffline
'https://docs.oracle.com/javase/8/docs/api' '/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/javadoc-bundle-options'
-use
-version
-windowtitle
'ROI to Distance Map 3.0.0 API'
plugins.adufour.distancetransforms
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/icy-kernel/2.4.2/icy-kernel-2.4.2.jar = 1660428539999
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/blocks/5.3.0/blocks-5.3.0.jar = 1660432057012
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/protocols/3.2.2/protocols-3.2.2.jar = 1660428530772
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/javadocparser/1.0.1/javadocparser-1.0.1.jar = 1660434797422
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/javacl/1.0.6/javacl-1.0.6.pom = 1660434083139
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/ezplug/3.21.0/ezplug-3.21.0.jar = 1660433664828
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/icy-insubstantial/7.3.10/icy-insubstantial-7.3.10.jar = 1660432121321
/home/kitwaii/.m2/repository/net/imagej/ij/1.53r/ij-1.53r.jar = 1660427240902
/home/kitwaii/.m2/repository/org/swinglabs/swingx/swingx-all/1.6.5-1/swingx-all-1.6.5-1.jar = 1660428645048
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/3d-mesh-roi/1.4.7/3d-mesh-roi-1.4.7.jar = 1660434907539
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/quickhull/1.0.2/quickhull-1.0.2.jar = 1660432144279
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/vecmath/1.6.1/vecmath-1.6.1.jar = 1660434632673
/home/kitwaii/.m2/repository/org/bioimageanalysis/icy/icy-bioformats/6.9.1/icy-bioformats-6.9.1.jar = 1660432038030
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/src/main/java = 1659970035311
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/generated-sources/annotations = 1660434907988
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/roi-to-distance-map-3.0.0.jar = 1660434909645
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/roi-to-distance-map-3.0.0-sources.jar = 1660434910047
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/package-list = 1660434911836
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/constant-values.html = 1660434911857
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/overview-tree.html = 1660434911882
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/index-all.html = 1660434911888
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/deprecated-list.html = 1660434911893
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/allclasses-frame.html = 1660434911895
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/allclasses-noframe.html = 1660434911896
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/index.html = 1660434911897
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/help-doc.html = 1660434911899
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/stylesheet.css = 1660434911901
/run/media/kitwaii/Dev/kitwaii/pasteur/Plugins/distance-transforms/target/script.js = 1660434911901
