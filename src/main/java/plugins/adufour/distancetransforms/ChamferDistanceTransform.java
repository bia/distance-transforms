package plugins.adufour.distancetransforms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import icy.common.Version;
import icy.image.IcyBufferedImage;
import icy.main.Icy;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.sequence.Sequence;
import icy.sequence.SequenceDataIterator;
import icy.type.DataIteratorUtil;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import icy.util.OMEUtil;
import plugins.adufour.roi.mesh.ROI3DMesh;
import plugins.kernel.roi.roi3d.ROI3DArea;
import plugins.kernel.roi.roi3d.ROI3DStack;

public abstract class ChamferDistanceTransform extends DistanceTransform
{
    
    @Override
    public Sequence createDistanceMap(Sequence input, int channel, double threshold, boolean invertMap, boolean useRealUnits)
    {
        int width = input.getSizeX();
        int height = input.getSizeY();
        int depth = input.getSizeZ();
        int time = input.getSizeT();
        DataType type = input.getDataType_();
        
        int maxDistance = width * height * depth;
        
        Sequence output = new Sequence("Distance map of " + input.getName());
        
        for (int t = 0; t < time; t++)
        {
            for (int z = 0; z < depth; z++)
                output.setImage(t, z, new IcyBufferedImage(width, height, 1, DataType.FLOAT));
            
            float[][] map = output.getDataXYZAsFloat(t, 0);
            
            // Initialize the distance map according to the threshold
            for (int z = 0; z < depth; z++)
            {
                float[] mapSlice = map[z];
                Object in = input.getDataXY(t, z, channel);
                
                if (invertMap)
                {
                    for (int i = 0; i < mapSlice.length; i++)
                        if (Array1DUtil.getValue(in, i, type) > threshold) mapSlice[i] = maxDistance;
                }
                else
                {
                    for (int i = 0; i < mapSlice.length; i++)
                        if (Array1DUtil.getValue(in, i, type) <= threshold) mapSlice[i] = maxDistance;
                }
            }
        }
        
        updateUnsignedChamferDistance(output, useRealUnits);
        
        output.dataChanged();
        return output;
    }
    
    /**
     * Creates a distance map from the ROI contained in the specified sequence. The resulting
     * distance map is a new sequence with one channel of type float and of same 4D dimensions as
     * the original sequence
     * 
     * @param sequence
     *            the sequence where ROI should be found and mapped
     * @param invertMap
     *            <code>true</code> will map in the inside of the ROI instead of the outside
     * @param useRealUnits
     *            <code>true</code> will return map values in real (metric) units, i.e. taking into
     *            account the pixel size
     * @return sequence
     */
    public Sequence createDistanceMap(Sequence sequence, boolean mapOnlySelectedROI, boolean invertMap, boolean useRealUnits) throws InterruptedException {
        List<ROI> rois = mapOnlySelectedROI ? sequence.getSelectedROIs() : sequence.getROIs();
        
        String title = "Distance map ";
        title += "(in " + (useRealUnits ? "microns)" : "pixels)");
        title += " to ROI from " + sequence.getName();
        
        final Sequence output = new Sequence(OMEUtil.createOMEXMLMetadata(sequence.getOMEXMLMetadata()), title);
        
        final int width = sequence.getSizeX();
        int height = sequence.getSizeY();
        int depth = sequence.getSizeZ();
        int length = sequence.getSizeT();
        
        int maxDistance = width * height * depth;
        
        // Initialize the distance map
        for (int t = 0; t < length; t++)
            for (int z = 0; z < depth; z++)
            {
                float[] slice = new float[width * height];
                if (!invertMap) Arrays.fill(slice, maxDistance);
                output.setImage(t, z, new IcyBufferedImage(width, height, new float[][] { slice }));
            }
        
        // Reverse the initial distance if necessary
        if (!invertMap) maxDistance = 0;
        
        // Fill the ROI with the default distance
        // Warning: Icy <= 1.8.6 have an issue with the data iterator
        Version patch = new Version(1, 8, 6);
        
        for (ROI r : rois)
        {
            if (Icy.version.isLowerOrEqual(patch) && r instanceof ROI3DStack)
            {
                ROI3DStack<?> stack = (ROI3DStack<?>) r;
                for (ROI2D slice : stack)
                {
                    SequenceDataIterator iterator = new SequenceDataIterator(output, slice, true);
                    DataIteratorUtil.set(iterator, maxDistance);
                }
            }
            else if (r instanceof ROI3DMesh)
            {
                ROI3D rea = new ROI3DArea(((ROI3D) r).getBooleanMask(true));
                rea.setC(((ROI3D) r).getC());
                rea.setT(((ROI3D) r).getT());
                SequenceDataIterator iterator = new SequenceDataIterator(output, r, true);
                DataIteratorUtil.set(iterator, maxDistance);
            }
            else
            {
                SequenceDataIterator iterator = new SequenceDataIterator(output, r, true);
                DataIteratorUtil.set(iterator, maxDistance);
            }
        }
        
        updateUnsignedChamferDistance(output, useRealUnits);
        
        output.dataChanged();
        return output;
    }
    
    private void updateUnsignedChamferDistance(final Sequence output, boolean useRealUnits)
    {
        final int width = output.getSizeX();
        final int length = output.getSizeT();
        
        final float stepXY = (float) (useRealUnits ? output.getPixelSizeX() : 1f);
        final float stepZ = (float) (useRealUnits ? output.getPixelSizeZ() : 1f);
        
        // Calculate the distance map
        ArrayList<Future<?>> results = new ArrayList<Future<?>>(length);
        
        for (int t = 0; t < length; t++)
        {
            final int time = t;
            
            results.add(multiThreadService.submit(new Callable<Object>()
            {
                @Override
                public Object call() throws Exception
                {
                    float[][] map = output.getDataXYZAsFloat(time, 0);
                    
                    if (map.length == 1)
                    {
                        updateUnsignedChamferDistance2D(map[0], width, stepXY);
                    }
                    else
                    {
                        updateUnsignedChamferDistance3D(map, width, stepXY, stepZ);
                    }
                    
                    return null;
                }
            }));
        }
        
        try
        {
            for (Future<?> future : results)
                future.get();
        }
        catch (Exception e)
        {
            
        }
        
    }
    
    /**
     * Updates an already-initialized 2D unsigned distance map (background value should be
     * infinity). The final map is given in the same units as given by the <code>pixelWidth</code>
     * parameter.
     * 
     * @param map
     *            the initialized map
     * @param lineSizeInPixels
     *            the width of the map
     * @param pixelWidth
     *            the width of the pixel (indicate <code>1</code> to obtain a pixel-based distance
     *            map, or the pixel size to obtain a distance map in real _metric_ units)
     */
    public abstract void updateUnsignedChamferDistance2D(final float[] map, int lineSizeInPixels, float pixelWidth);
    
    /**
     * Updates an already-initialized 3D unsigned distance map (background value should be infinity)
     * 
     * @param map
     *            the initialized map
     * @param lineSizeInPixels
     *            the width of the map
     * @param planarPixelSize
     *            the planar width of each voxel (indicate <code>1</code> to obtain a voxel-based
     *            distance map, or the voxel width along the XY axis to obtain a distance map in
     *            real _metric_ units)
     *            the axial width of each voxel (indicate <code>1</code> to obtain a voxel-based
     *            distance map, or the voxel width along the Z axis to obtain a distance map in real
     *            _metric_ units)
     */
    public abstract void updateUnsignedChamferDistance3D(final float[][] map, int lineSizeInPixels, float planarPixelSize, float axialPixelSize);
    
}
