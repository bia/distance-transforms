package plugins.adufour.distancetransforms;

import icy.sequence.Sequence;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarPlugin;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarSequence;

public class ROIToDistanceMap extends EzPlug implements Block
{
    EzVarSequence sequence = new EzVarSequence("Sequence");

    EzVarBoolean selectedOnly = new EzVarBoolean("Restrict to selected ROI", false);

    EzVarPlugin<DistanceTransform> transform = new EzVarPlugin<DistanceTransform>("Algorithm", DistanceTransform.class);

    EzVarBoolean invert = new EzVarBoolean("Invert map", true);

    EzVarBoolean useRealUnits = new EzVarBoolean("Use pixel size", false);

    VarSequence map = new VarSequence("Distance map", null);

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Use ROI from", sequence.getVariable());
        inputMap.add("Algorithm", transform.getVariable());
        inputMap.add("Invert map", invert.getVariable());
        inputMap.add("Real units", useRealUnits.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Distance map", map);
    }

    @Override
    protected void initialize()
    {
        addEzComponent(sequence);
        addEzComponent(transform);
        addEzComponent(selectedOnly);
        addEzComponent(invert);
        addEzComponent(useRealUnits);

        setTimeDisplay(true);
    }

    @Override
    protected void execute()
    {
        try {
            map.setValue(transform.newInstance().createDistanceMap(sequence.getValue(true), selectedOnly.getValue(),
                    !invert.getValue(), useRealUnits.getValue()));
        }
        catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (!isHeadLess())
        {
            Sequence mapSequence = map.getValue();
            mapSequence.updateChannelsBounds();
            addSequence(mapSequence);
        }
    }

    @Override
    public void clean()
    {
    }

}
