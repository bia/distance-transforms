package plugins.adufour.distancetransforms;

/**
 * Pseudo-Euclidean distance transform based on a two-pass sweeping algorithm using a 5x5 (in 2D) or
 * 5x5x5 (in 3D) chamfer mask. <br>
 * The chamfer mask defines distances as follows:
 * 
 * <pre>
 *  Z = 0 (or 2D):      Z = +/- 1:      Z = +/- 2:
 *    . c . c .         h g i g h       . l . l .
 *    c b a b c         g f e f g       l k j k l 
 *    . a x a .         i e d e i       . j . j .
 *    c b a b c         g f e f g       l k j k l
 *    . c . c .         h g i g h       . l . l .
 * </pre>
 * 
 * In the default (Euclidean case), we have:
 * 
 * <pre>
 * a=d=1  b=e=sqrt(2)  c=j=sqrt(5)
 * </pre>
 * 
 * @author Alexandre Dufour
 */
public class Chamfer5 extends ChamferDistanceTransform
{
    public void updateUnsignedChamferDistance2D(float[] map, int width, float lateralDistance)
    {
        // pre-calculate distances
        float a = lateralDistance;
        float b = (float) Math.sqrt(a * a + a * a);
        float c = (float) Math.sqrt(a * a + 4 * a * a);
        
        int height = map.length / width;
        int index = 0;
        float aDist, bDist, dDist;
        
        // First pass
        
        for (int j = 0; j < height; j++)
            for (int i = 0; i < width; i++, index++)
            {
                float val = map[index];
                aDist = val + a;
                
                if (i < width - 1 && map[index + 1] > aDist) map[index + 1] = aDist;
                
                if (j < height - 1)
                {
                    bDist = val + b;
                    dDist = val + c;
                    
                    if (map[index + width] > aDist) map[index + width] = aDist;
                    
                    if (i < width - 1)
                    {
                        if (map[index + width + 1] > bDist) map[index + width + 1] = bDist;
                        
                        if (i < width - 2 && map[index + width + 2] > dDist) map[index + width + 2] = dDist;
                        
                        if (j < height - 2 && map[index + width + width + 1] > dDist) map[index + width + width + 1] = dDist;
                    }
                    
                    if (i > 0)
                    {
                        if (map[index + width - 1] > bDist) map[index + width - 1] = bDist;
                        
                        if (i > 1 && map[index + width - 2] > dDist) map[index + width - 2] = dDist;
                        
                        if (j < height - 2 && map[index + width + width - 1] > dDist) map[index + width + width - 1] = dDist;
                    }
                }
            }
        
        index--;
        
        // Second pass
        
        for (int j = height - 1; j >= 0; j--)
            for (int i = width - 1; i >= 0; i--, index--)
            {
                float val = map[index];
                aDist = val + a;
                
                if (i > 0 && map[index - 1] > aDist) map[index - 1] = aDist;
                
                if (j > 0)
                {
                    bDist = val + b;
                    dDist = val + c;
                    
                    if (map[index - width] > aDist) map[index - width] = aDist;
                    
                    if (i > 0)
                    {
                        if (map[index - width - 1] > bDist) map[index - width - 1] = bDist;
                        
                        if (i > 1 && map[index - width - 2] > dDist) map[index - width - 2] = dDist;
                        
                        if (j > 1 && map[index - width - width - 1] > dDist) map[index - width - width - 1] = dDist;
                    }
                    
                    if (i < width - 1)
                    {
                        if (map[index - width + 1] > bDist) map[index - width + 1] = bDist;
                        
                        if (i < width - 2 && map[index - width + 2] > dDist) map[index - width + 2] = dDist;
                        
                        if (j > 1 && map[index - width - width + 1] > dDist) map[index - width - width + 1] = dDist;
                    }
                }
            }
    }
    
    public void updateUnsignedChamferDistance3D(final float[][] map, int width, float planarLateralDistance, float axialLateralDistance)
    {
        // 2D distances
        float xySq = planarLateralDistance * planarLateralDistance;
        float b = (float) Math.sqrt(xySq + xySq);
        float c = (float) Math.sqrt(xySq + 4 * xySq);
        
        // in 3D
        float zSq = axialLateralDistance * axialLateralDistance;
        // Z +/- 1
        float d = axialLateralDistance;
        float e = (float) Math.sqrt(zSq + xySq);
        float f = (float) Math.sqrt(zSq + xySq + xySq);
        float g = (float) Math.sqrt(zSq + 4 * xySq + xySq);
        float h = (float) Math.sqrt(zSq + 4 * xySq + 4 * xySq);
        float i = (float) Math.sqrt(zSq + 4 * xySq);
        // Z +/- 1
        float j = (float) Math.sqrt(4 * zSq + xySq);
        float k = (float) Math.sqrt(4 * zSq + xySq + xySq);
        float l = (float) Math.sqrt(4 * zSq + 4 * xySq + xySq);
        
        float aDist, bDist, cDist, dDist, eDist, fDist, gDist, hDist, iDist, jDist, kDist, lDist;
        
        int height = map[0].length / width;
        int depth = map.length;
        
        // First pass
        
        for (int kk = 0; kk < depth; kk++)
        {
            float[] z0 = map[kk];
            float[] z1 = (kk < depth - 1) ? map[kk + 1] : null;
            float[] z2 = (kk < depth - 2) ? map[kk + 2] : null;
            
            int offset = 0;
            
            for (int jj = 0; jj < height; jj++)
                for (int ii = 0; ii < width; ii++, offset++)
                {
                    float val = z0[offset];
                    aDist = val + planarLateralDistance;
                    bDist = val + b;
                    cDist = val + c;
                    dDist = val + d;
                    eDist = val + e;
                    fDist = val + f;
                    gDist = val + g;
                    hDist = val + h;
                    iDist = val + i;
                    jDist = val + j;
                    kDist = val + k;
                    lDist = val + l;
                    
                    int top = offset - width;
                    int top_top = top - width;
                    int bottom = offset + width;
                    int bottom_bottom = bottom + width;
                    
                    if (kk < depth - 1)
                    {
                        // Z = 1
                        
                        if (ii >= 2 && z1[offset - 2] > iDist) z1[offset - 2] = iDist;//
                        if (ii >= 1 && z1[offset - 1] > eDist) z1[offset - 1] = eDist;//
                        if (z1[offset] > dDist) z1[offset] = dDist;//
                        if (ii < width - 1 && z1[offset + 1] > eDist) z1[offset + 1] = eDist;//
                        if (ii < width - 2 && z1[offset + 2] > iDist) z1[offset + 2] = iDist;//
                            
                        if (jj >= 1)
                        {
                            if (ii >= 2 && z1[top - 2] > gDist) z1[top - 2] = gDist;//
                            if (ii >= 1 && z1[top - 1] > fDist) z1[top - 1] = fDist;//
                            if (z1[top] > eDist) z1[top] = eDist;//
                            if (ii < width - 1 && z1[top + 1] > fDist) z1[top + 1] = fDist;//
                            if (ii < width - 2 && z1[top + 2] > gDist) z1[top + 2] = gDist;//
                                
                            if (jj >= 2)
                            {
                                if (ii >= 2 && z1[top_top - 2] > hDist) z1[top_top - 2] = hDist;//
                                if (ii >= 1 && z1[top_top - 1] > gDist) z1[top_top - 1] = gDist;//
                                if (z1[top_top] > iDist) z1[top_top] = iDist;//
                                if (ii < width - 1 && z1[top_top + 1] > gDist) z1[top_top + 1] = gDist;//
                                if (ii < width - 2 && z1[top_top + 2] > hDist) z1[top_top + 2] = hDist;//
                            }
                        }
                        
                        if (jj < height - 1)
                        {
                            if (ii >= 2 && z1[bottom - 2] > gDist) z1[bottom - 2] = gDist;//
                            if (ii >= 1 && z1[bottom - 1] > fDist) z1[bottom - 1] = fDist;//
                            if (z1[bottom] > eDist) z1[bottom] = eDist;//
                            if (ii < width - 1 && z1[bottom + 1] > fDist) z1[bottom + 1] = fDist;//
                            if (ii < width - 2 && z1[bottom + 2] > gDist) z1[bottom + 2] = gDist;//
                                
                            if (jj < height - 2)
                            {
                                if (ii > 1 && z1[bottom_bottom - 2] > hDist) z1[bottom_bottom - 2] = hDist;//
                                if (ii > 0 && z1[bottom_bottom - 1] > gDist) z1[bottom_bottom - 1] = gDist;//
                                if (z1[bottom_bottom] > iDist) z1[bottom_bottom] = iDist;//
                                if (ii < width - 1 && z1[bottom_bottom + 1] > gDist) z1[bottom_bottom + 1] = gDist;//
                                if (ii < width - 2 && z1[bottom_bottom + 2] > hDist) z1[bottom_bottom + 2] = hDist;//
                            }
                        }
                        
                        if (kk < depth - 2)
                        {
                            // Z = 2
                            if (jj >= 1)
                            {
                                if (ii > 1 && z2[top - 2] > lDist) z2[top - 2] = lDist;//
                                if (ii > 0 && z2[top - 1] > kDist) z2[top - 1] = kDist;//
                                if (z2[top] > jDist) z2[top] = jDist;//
                                if (ii < width - 1 && z2[top + 1] > kDist) z2[top + 1] = kDist;//
                                if (ii < width - 2 && z2[top + 2] > lDist) z2[top + 2] = lDist;//
                                    
                                if (jj >= 2)
                                {
                                    if (ii > 0 && z2[top_top - 1] > lDist) z2[top_top - 1] = lDist;//
                                    if (ii < width - 1 && z2[top_top + 1] > lDist) z2[top_top + 1] = lDist;//
                                }
                            }
                            
                            if (ii >= 1 && z2[offset - 1] > jDist) z2[offset - 1] = jDist;//
                            if (ii < width - 1 && z2[offset + 1] > jDist) z2[offset + 1] = jDist;//
                                
                            if (jj < height - 1)
                            {
                                if (ii >= 2 && z2[bottom - 2] > lDist) z2[bottom - 2] = lDist;//
                                if (ii >= 1 && z2[bottom - 1] > kDist) z2[bottom - 1] = kDist;//
                                if (z2[bottom] > jDist) z2[bottom] = jDist;//
                                if (ii < width - 1 && z2[bottom + 1] > kDist) z2[bottom + 1] = kDist;//
                                if (ii < width - 2 && z2[bottom + 2] > lDist) z2[bottom + 2] = lDist;//
                                    
                                if (jj < height - 2)
                                {
                                    if (ii >= 1 && z2[bottom_bottom - 1] > lDist) z2[bottom_bottom - 1] = lDist;//
                                    if (ii < width - 1 && z2[bottom_bottom + 1] > lDist) z2[bottom_bottom + 1] = lDist;//
                                }
                            }
                        }
                    }
                    
                    // Z = 0
                    //
                    // . c . c .
                    // c b a b c
                    // . a x a .
                    // c b a b c
                    // . c . c .
                    //
                    if (jj < height - 1)
                    {
                        if (ii < width - 1)
                        {
                            if (z0[bottom + 1] > bDist) z0[bottom + 1] = bDist;//
                            if (jj < height - 2 && z0[bottom_bottom + 1] > cDist) z0[bottom_bottom + 1] = cDist;//
                        }
                        
                        if (ii < width - 2 && z0[bottom + 2] > cDist) z0[bottom + 2] = cDist;//
                        if (z0[bottom] > aDist) z0[bottom] = aDist;//
                            
                        if (ii >= 1)
                        {
                            if (z0[bottom - 1] > bDist) z0[bottom - 1] = bDist;
                            if (jj < height - 2 && z0[bottom_bottom - 1] > cDist) z0[bottom_bottom - 1] = cDist;//
                        }
                        
                        if (ii >= 2 && z0[bottom - 2] > cDist) z0[bottom - 2] = cDist;//
                    }
                    
                    if (ii < width - 1 && z0[offset + 1] > aDist) z0[offset + 1] = aDist;//
                }
        }
        
        // Icy.getMainInterface().addSequence(new Sequence(new IcyBufferedImage(width, height, new
        // float[][]{map[9]})));
        
        // Second pass
        
        for (int kk = depth - 1; kk >= 0; kk--)
        {
            float[] z0 = map[kk];
            float[] z1 = (kk >= 1) ? map[kk - 1] : null;
            float[] z2 = (kk >= 2) ? map[kk - 2] : null;
            
            int offset = z0.length - 1;
            
            for (int jj = height - 1; jj >= 0; jj--)
                for (int ii = width - 1; ii >= 0; ii--, offset--)
                {
                    float val = z0[offset];
                    aDist = val + planarLateralDistance;
                    bDist = val + b;
                    cDist = val + c;
                    dDist = val + d;
                    eDist = val + e;
                    fDist = val + f;
                    gDist = val + g;
                    hDist = val + h;
                    iDist = val + i;
                    jDist = val + j;
                    kDist = val + k;
                    lDist = val + l;
                    
                    int top = offset - width;
                    int top_top = top - width;
                    int bottom = offset + width;
                    int bottom_bottom = bottom + width;
                    
                    if (kk >= 1)
                    {
                        // Z = 1
                        
                        if (jj >= 1)
                        {
                            if (ii >= 2 && z1[top - 2] > gDist) z1[top - 2] = gDist;//
                            if (ii >= 1 && z1[top - 1] > fDist) z1[top - 1] = fDist;//
                            if (z1[top] > eDist) z1[top] = eDist;//
                            if (ii < width - 1 && z1[top + 1] > fDist) z1[top + 1] = fDist;//
                            if (ii < width - 2 && z1[top + 2] > gDist) z1[top + 2] = gDist;//
                                
                            if (jj >= 2)
                            {
                                if (ii >= 2 && z1[top_top - 2] > hDist) z1[top_top - 2] = hDist;//
                                if (ii >= 1 && z1[top_top - 1] > gDist) z1[top_top - 1] = gDist;//
                                if (z1[top_top] > iDist) z1[top_top] = iDist;//
                                if (ii < width - 1 && z1[top_top + 1] > gDist) z1[top_top + 1] = gDist;//
                                if (ii < width - 2 && z1[top_top + 2] > hDist) z1[top_top + 2] = hDist;//
                            }
                        }
                        
                        if (ii >= 2 && z1[offset - 2] > iDist) z1[offset - 2] = iDist;//
                        if (ii >= 1 && z1[offset - 1] > eDist) z1[offset - 1] = eDist;//
                        if (z1[offset] > dDist) z1[offset] = dDist;//
                        if (ii < width - 1 && z1[offset + 1] > eDist) z1[offset + 1] = eDist;//
                        if (ii < width - 2 && z1[offset + 2] > iDist) z1[offset + 2] = iDist;//
                            
                        if (jj < height - 1)
                        {
                            if (ii >= 2 && z1[bottom - 2] > gDist) z1[bottom - 2] = gDist;//
                            if (ii >= 1 && z1[bottom - 1] > fDist) z1[bottom - 1] = fDist;//
                            if (z1[bottom] > eDist) z1[bottom] = eDist;//
                            if (ii < width - 1 && z1[bottom + 1] > fDist) z1[bottom + 1] = fDist;//
                            if (ii < width - 2 && z1[bottom + 2] > gDist) z1[bottom + 2] = gDist;//
                                
                            if (jj < height - 2)
                            {
                                if (ii >= 2 && z1[bottom_bottom - 2] > hDist) z1[bottom_bottom - 2] = hDist;//
                                if (ii >= 1 && z1[bottom_bottom - 1] > gDist) z1[bottom_bottom - 1] = gDist;//
                                if (z1[bottom_bottom] > iDist) z1[bottom_bottom] = iDist;//
                                if (ii < width - 1 && z1[bottom_bottom + 1] > gDist) z1[bottom_bottom + 1] = gDist;//
                                if (ii < width - 2 && z1[bottom_bottom + 2] > hDist) z1[bottom_bottom + 2] = hDist;//
                            }
                        }
                        
                        if (kk > 1)
                        {
                            // Z = 2
                            if (jj > 0)
                            {
                                if (ii >= 2 && z2[top - 2] > lDist) z2[top - 2] = lDist;//
                                if (ii >= 1 && z2[top - 1] > kDist) z2[top - 1] = kDist;//
                                if (z2[top] > jDist) z2[top] = jDist;//
                                if (ii < width - 1 && z2[top + 1] > kDist) z2[top + 1] = kDist;//
                                if (ii < width - 2 && z2[top + 2] > lDist) z2[top + 2] = lDist;//
                                    
                                if (jj > 1)
                                {
                                    if (ii >= 1 && z2[top_top - 1] > lDist) z2[top_top - 1] = lDist;//
                                    if (ii < width - 1 && z2[top_top + 1] > lDist) z2[top_top + 1] = lDist;//
                                }
                            }
                            
                            if (ii >= 1 && z2[offset - 1] > jDist) z2[offset - 1] = jDist;//
                            if (ii < width - 1 && z2[offset + 1] > jDist) z2[offset + 1] = jDist;//
                                
                            if (jj < height - 1)
                            {
                                if (ii >= 2 && z2[bottom - 2] > lDist) z2[bottom - 2] = lDist;//
                                if (ii >= 1 && z2[bottom - 1] > kDist) z2[bottom - 1] = kDist;//
                                if (z2[bottom] > jDist) z2[bottom] = jDist;//
                                if (ii < width - 1 && z2[bottom + 1] > kDist) z2[bottom + 1] = kDist;//
                                if (ii < width - 2 && z2[bottom + 2] > lDist) z2[bottom + 2] = lDist;//
                                    
                                if (jj < height - 2)
                                {
                                    if (ii >= 1 && z2[bottom_bottom - 1] > lDist) z2[bottom_bottom - 1] = lDist;//
                                    if (ii < width - 1 && z2[bottom_bottom + 1] > lDist) z2[bottom_bottom + 1] = lDist;//
                                }
                            }
                        }
                    }
                    
                    // Z = 0
                    //
                    // . c . c .
                    // c b a b c
                    // . a x a .
                    // c b a b c
                    // . c . c .
                    //
                    if (jj > 0)
                    {
                        if (ii >= 2 && z0[top - 2] > cDist) z0[top - 2] = cDist;//
                        if (ii >= 1 && z0[top - 1] > bDist) z0[top - 1] = bDist;//
                        if (z0[top] > aDist) z0[top] = aDist;//
                        if (ii < width - 1 && z0[top + 1] > bDist) z0[top + 1] = bDist;//
                        if (ii < width - 2 && z0[top + 2] > cDist) z0[top + 2] = cDist;//
                            
                        if (jj > 1)
                        {
                            if (ii >= 1 && z0[top_top - 1] > cDist) z0[top_top - 1] = cDist;//
                            if (ii < width - 1 && z0[top_top + 1] > cDist) z0[top_top + 1] = cDist;//
                        }
                    }
                    
                    if (ii > 0 && z0[offset - 1] > aDist) z0[offset - 1] = aDist;//
                }
        }
    }
}
