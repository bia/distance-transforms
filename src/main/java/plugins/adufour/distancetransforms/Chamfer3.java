package plugins.adufour.distancetransforms;

/**
 * Pseudo-Euclidean distance transform based on a two-pass sweeping algorithm using a 3x3 (in 2D) or
 * 3x3x3 (in 3D) chamfer mask. <br>
 * The chamfer mask defines distances as follows:
 * 
 * <pre>
 *                  b a b
 *  Z = 0 (or 2D):  a x a
 *                  b a b
 * 
 *                  e d e
 *  Z = +/- 1:      d c d
 *                  e d e
 * </pre>
 * 
 * @author Alexandre Dufour
 */
public class Chamfer3 extends ChamferDistanceTransform
{
    public void updateUnsignedChamferDistance2D(float[] map, int width, float planarLateralDistance)
    {
        double xy2 = planarLateralDistance * planarLateralDistance;
        float planarDiagonalDistance = (float) Math.sqrt(xy2 + xy2);
        
        int height = map.length / width;
        int i, j, offset = 0;
        float val, aDist, bDist;
        
        // First pass (forward)
        // . . .
        // . x a
        // b a b
        
        for (j = 0; j < height; j++)
            for (i = 0; i < width; i++, offset++)
            {
                val = map[offset];
                
                aDist = val + planarLateralDistance;
                
                if (i < width - 1 && map[offset + 1] > aDist) map[offset + 1] = aDist;
                
                if (j < height - 1)
                {
                    if (map[offset + width] > aDist) map[offset + width] = aDist;
                    
                    bDist = val + planarDiagonalDistance;
                    
                    if (i < width - 1 && map[offset + width + 1] > bDist) map[offset + width + 1] = bDist;
                    
                    if (i > 0 && map[offset + width - 1] > bDist) map[offset + width - 1] = bDist;
                }
            }
        
        offset--;
        
        // Second pass (backward)
        // b a b
        // a x .
        // . . .
        
        for (j = height - 1; j >= 0; j--)
            for (i = width - 1; i >= 0; i--, offset--)
            {
                aDist = map[offset] + planarLateralDistance;
                
                if (i > 0 && map[offset - 1] > aDist) map[offset - 1] = aDist;
                
                if (j > 0)
                {
                    bDist = aDist - planarLateralDistance + planarDiagonalDistance;
                    
                    if (map[offset - width] > aDist) map[offset - width] = aDist;
                    
                    if (i > 0 && map[offset - width - 1] > bDist) map[offset - width - 1] = bDist;
                    
                    if (i < width - 1 && map[offset - width + 1] > bDist) map[offset - width + 1] = bDist;
                }
            }
    }
    
    public void updateUnsignedChamferDistance3D(float[][] map, int width, float planoLateralDistance, float axioLateralDistance)
    {
        double xy2 = planoLateralDistance * planoLateralDistance;
        double xz2 = axioLateralDistance * axioLateralDistance;
        float planoDiagonalDistance = (float) Math.sqrt(xy2 + xy2);
        float axioDiagonalDistance = (float) Math.sqrt(xy2 + xz2);
        float planoAxioDiagonalDistance = (float) Math.sqrt(xy2 + xy2 + xz2);
        
        int height = map[0].length / width;
        int depth = map.length;
        
        int i, j, k;
        
        // First pass (forward)
        
        for (k = 0; k < depth; k++)
        {
            float[] thisSlice = map[k];
            
            int offset = 0;
            
            for (j = 0; j < height; j++)
                for (i = 0; i < width; i++, offset++)
                {
                    float val = thisSlice[offset];
                    
                    // Current slice:
                    // . . .
                    // . x a
                    // b a b
                    
                    float aDist = val + planoLateralDistance;
                    
                    if (i < width - 1 && thisSlice[offset + 1] > aDist) thisSlice[offset + 1] = aDist;
                    
                    if (j < height - 1)
                    {
                        if (thisSlice[offset + width] > aDist) thisSlice[offset + width] = aDist;
                        
                        float bDist = val + planoDiagonalDistance;
                        
                        if (i < width - 1 && thisSlice[offset + width + 1] > bDist) thisSlice[offset + width + 1] = bDist;
                        
                        if (i > 0 && thisSlice[offset + width - 1] > bDist) thisSlice[offset + width - 1] = bDist;
                    }
                    
                    if (k < depth - 1)
                    {
                        float[] nextSlice = map[k + 1];
                        
                        // Next slice:
                        // e d e
                        // d c d
                        // e d e
                        
                        float cDist = val + axioLateralDistance;
                        
                        if (nextSlice[offset] > cDist) nextSlice[offset] = cDist;
                        
                        float dDist = val + axioDiagonalDistance;
                        
                        // left
                        if (i > 0 && nextSlice[offset - 1] > dDist) nextSlice[offset - 1] = dDist;
                        
                        // right
                        if (i < width - 1 && nextSlice[offset + 1] > dDist) nextSlice[offset + 1] = dDist;
                        
                        float eDist = val + planoAxioDiagonalDistance;
                        
                        if (j > 0)
                        {
                            int offsetUp = offset - width;
                            
                            // up
                            if (nextSlice[offsetUp] > dDist) nextSlice[offsetUp] = dDist;
                            // up-left
                            if (i > 0 && nextSlice[offsetUp - 1] > eDist) nextSlice[offsetUp - 1] = eDist;
                            // up-right
                            if (i < width - 1 && nextSlice[offsetUp + 1] > eDist) nextSlice[offsetUp + 1] = eDist;
                        }
                        
                        if (j < height - 1)
                        {
                            int offsetDown = offset + width;
                            
                            // down
                            if (nextSlice[offsetDown] > dDist) nextSlice[offsetDown] = dDist;
                            // down-left
                            if (i > 0 && nextSlice[offsetDown - 1] > eDist) nextSlice[offsetDown - 1] = eDist;
                            // down-right
                            if (i < width - 1 && nextSlice[offsetDown + 1] > eDist) nextSlice[offsetDown + 1] = eDist;
                        }
                    }
                    
                }
        }
        
        // Second pass
        
        for (k = depth - 1; k >= 0; k--)
        {
            float[] thisSlice = map[k];
            
            int offset = thisSlice.length - 1;
            
            for (j = height - 1; j >= 0; j--)
                for (i = width - 1; i >= 0; i--, offset--)
                {
                    float val = thisSlice[offset];
                    
                    // Current slice:
                    // b a b
                    // a x .
                    // . . .
                    
                    float aDist = val + planoLateralDistance;
                    
                    // left
                    if (i > 0 && thisSlice[offset - 1] > aDist) thisSlice[offset - 1] = aDist;
                    
                    if (j > 0)
                    {
                        int offsetUp = offset - width;
                        
                        // up
                        if (thisSlice[offsetUp] > aDist) thisSlice[offsetUp] = aDist;
                        
                        float bDist = val + planoDiagonalDistance;
                        
                        // up-left
                        if (i > 0 && thisSlice[offsetUp - 1] > bDist) thisSlice[offsetUp - 1] = bDist;
                        // up-right
                        if (i < width - 1 && thisSlice[offsetUp + 1] > bDist) thisSlice[offsetUp + 1] = bDist;
                    }
                    
                    if (k > 0)
                    {
                        float[] prevSlice = map[k - 1];
                        
                        // Previous slice:
                        // e d e
                        // d c d
                        // e d e
                        
                        float cDist = val + axioLateralDistance;
                        
                        if (prevSlice[offset] > cDist) prevSlice[offset] = cDist;
                        
                        float dDist = val + axioDiagonalDistance;
                        
                        // left
                        if (i > 0 && prevSlice[offset - 1] > dDist) prevSlice[offset - 1] = dDist;
                        
                        // right
                        if (i < width - 1 && prevSlice[offset + 1] > dDist) prevSlice[offset + 1] = dDist;
                        
                        float eDist = val + planoAxioDiagonalDistance;
                        
                        if (j > 0)
                        {
                            int offsetUp = offset - width;
                            
                            // up
                            if (prevSlice[offsetUp] > dDist) prevSlice[offsetUp] = dDist;
                            // up-left
                            if (i > 0 && prevSlice[offsetUp - 1] > eDist) prevSlice[offsetUp - 1] = eDist;
                            // up-right
                            if (i < width - 1 && prevSlice[offsetUp + 1] > eDist) prevSlice[offsetUp + 1] = eDist;
                        }
                        
                        if (j < height - 1)
                        {
                            int offsetDown = offset + width;
                            
                            // down
                            if (prevSlice[offsetDown] > dDist) prevSlice[offsetDown] = dDist;
                            // down-left
                            if (i > 0 && prevSlice[offsetDown - 1] > eDist) prevSlice[offsetDown - 1] = eDist;
                            // down-right
                            if (i < width - 1 && prevSlice[offsetDown + 1] > eDist) prevSlice[offsetDown + 1] = eDist;
                        }
                    }
                    
                }
        }
    }
    
}
