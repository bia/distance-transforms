package plugins.adufour.distancetransforms;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;

public abstract class DistanceTransform extends Plugin implements PluginLibrary
{
    protected static ExecutorService multiThreadService = Executors.newCachedThreadPool();
    
    /**
     * Creates a distance map from a sequence, where background and foreground are separated by an
     * intensity threshold
     * 
     * @param input
     *            the input sequence
     * @param channel
     *            the channel to process
     * @param threshold
     *            A threshold separating foreground and background regions in the specified channel
     *            (foreground is defined by values strictly above the threshold, i.e. use 0 on a
     *            binary image)
     * @param invertMap
     *            by default, distances are calculated from background to foreground (positive map
     *            values in the background, 0 in the foreground). Set to <code>true</code> to invert
     *            the calculation (positive map values in the foreground, 0 in the background).
     * @param useRealUnits
     *            Set to <code>true</code> to calculate the map using real (metric) units, i.e.
     *            taking into account the pixel size, or false to default to conventional Euclidean
     *            distance map
     * @return a new, single-channel sequence of type float with same 4D dimensions as the original
     *         sequence
     */
    public abstract Sequence createDistanceMap(Sequence input, int channel, double threshold, boolean invertMap, boolean useRealUnits);
    
    /**
     * Creates a distance map from the ROI contained in the specified sequence. The resulting
     * distance map is a new sequence with one channel of type float and of same 4D dimensions as
     * the original sequence
     * 
     * @param sequence
     *            the sequence where ROI should be found and mapped
     * @param invertMap
     *            by default, distances are calculated from background to foreground (positive map
     *            values in the background, 0 in the foreground). Set to <code>true</code> to invert
     *            the calculation (positive map values in the foreground, 0 in the background).
     * @param useRealUnits
     *            Set to <code>true</code> to calculate the map using real (metric) units, i.e.
     *            taking into account the pixel size, or false to default to conventional Euclidean
     *            distance map
     * @return a new, single-channel sequence of type float with same 4D dimensions as the original
     *         sequence
     */
    public abstract Sequence createDistanceMap(Sequence sequence, boolean mapOnlySelectedROI, boolean invertMap, boolean useRealUnits) throws InterruptedException;
}
